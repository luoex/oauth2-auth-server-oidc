package com.luo.demo.security;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 启动应用
 *
 * @author luohq
 * @date 2022-01-15 12:01
 */
@SpringBootApplication
public class Oauth2Resource1JwtProdApplication {
    public static void main(String[] args) {
        //设置CA公钥证书
        System.setProperty("javax.net.ssl.trustStore", "D:/idea_workspace/gitee/luoex/oauth2-auth-server-oidc/scg-client-prod-sample/ssl/ca.p12");
        System.setProperty("javax.net.ssl.trustStorePassword", "123456");
        System.setProperty("javax.net.ssl.trustStoreType", "PKCS12");

        SpringApplication.run(Oauth2Resource1JwtProdApplication.class, args);
    }
}

package com.luo.demo.oidc.authserver;

import com.luo.sc.oidc.authserver.constant.Oauth2Constants.CLIENT_SETTINGS;
import com.luo.sc.oidc.authserver.constant.Oauth2Constants.TOKEN_SETTINGS;
import com.nimbusds.openid.connect.sdk.OIDCScopeValue;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.EnabledIfSystemProperty;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.core.AuthorizationGrantType;
import org.springframework.security.oauth2.core.ClientAuthenticationMethod;
import org.springframework.security.oauth2.core.oidc.OidcScopes;
import org.springframework.security.oauth2.jose.jws.SignatureAlgorithm;
import org.springframework.security.oauth2.server.authorization.client.JdbcRegisteredClientRepository;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClient;
import org.springframework.security.oauth2.server.authorization.config.ClientSettings;
import org.springframework.security.oauth2.server.authorization.config.TokenSettings;
import org.springframework.test.context.ActiveProfiles;

import javax.annotation.Resource;
import java.time.Duration;
import java.util.UUID;

/**
 * OIDC 相关测试
 *
 * @author luohq
 * @date 2022-03-05 11:22
 */
@Slf4j
@SpringBootTest
@ActiveProfiles({"oidc", "session"})
public class ClientRegistrationTest {

    @Resource
    private JdbcRegisteredClientRepository registeredClientRepository;

    @Resource
    private PasswordEncoder passwordEncoder;

    @ParameterizedTest
    @ValueSource(strings = {"oauth2-scg-client-prod-secret"})
    void testPasswordEncoder(String rawPassword) {
        log.info("rawPassword: {}, encodedPassword: {}", rawPassword, this.passwordEncoder.encode(rawPassword));
    }

    @Test
    //有条件测试，避免直接执行测试用例而导致无效插入
    @EnabledIfSystemProperty(named = "testRegOidcClient", matches = "true")
    void regOidcClient() {
        RegisteredClient registeredClient_client_scg = RegisteredClient.withId(UUID.randomUUID().toString())
                .clientName("oauth2-scg-client-prod - 客户端")
                //客户端id和secret
                .clientId("oauth2-scg-client-prod")
                .clientSecret("{bcrypt}$2a$10$PZKbU2mma/3cKcjxtnoJd.qNcQJfvqQzOUEaTqy2SfiNBKyrK6/xO")
                //支持的认证方法 - post, basic
                .clientAuthenticationMethod(ClientAuthenticationMethod.CLIENT_SECRET_POST)
                .clientAuthenticationMethod(ClientAuthenticationMethod.CLIENT_SECRET_BASIC)
                //支持的授权类型 - authorization_code, refresh_token
                .authorizationGrantType(AuthorizationGrantType.AUTHORIZATION_CODE)
                .authorizationGrantType(AuthorizationGrantType.REFRESH_TOKEN)
                //scg-client-prod客户端授权回调地址（携带code、state回调scg）
                //支持多回调域名
                //同时支持http和https
                .redirectUri("http://scg-client-prod:8080/login/oauth2/code/scg-client-prod")
                .redirectUri("https://scg-client-prod:8080/login/oauth2/code/scg-client-prod")
                //客户端支持的权限scope
                .scope(OidcScopes.OPENID)
                .scope(OidcScopes.PHONE)
                .scope(OidcScopes.EMAIL)
                .scope(OidcScopes.PROFILE)
                .scope("articles.read")
                .scope("roles")
                //客户端设置
                .clientSettings(ClientSettings.builder()
                        //禁用确认页面
                        .requireAuthorizationConsent(false)
                        .build())
                //客户端颁发令牌相关设置
                .tokenSettings(TokenSettings.builder()
                        //accessToken生存时长（即超过多久失效，默认5分钟）
                        .accessTokenTimeToLive(Duration.ofMinutes(5))
                        //refreshToken生存时长（即超过多久失效，默认60分钟）
                        .refreshTokenTimeToLive(Duration.ofMinutes(60))
                        //执行刷新token流程时，是否返回新的refreshToken（默认true即重用refreshToken），
                        //true则重用之前的refreshToken，false则生成新的refreshToken及生存时长
                        .reuseRefreshTokens(false)
                        .build())
                .build();


        registeredClientRepository.save(registeredClient_client_scg);
    }

}


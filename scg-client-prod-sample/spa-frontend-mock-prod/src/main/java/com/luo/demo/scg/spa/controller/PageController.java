package com.luo.demo.scg.spa.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * 页面controller
 *
 * @author luohq
 * @version 1.0.0
 * @date 2022-02-14 14:52
 */
@Controller
@Slf4j
public class PageController {

    @GetMapping("/")
    public String index(Model model) {
        String welcomeMsg = "SPA主页";
        log.info("cur welcomeMsg: {}", welcomeMsg);
        model.addAttribute("welcomeMsg", welcomeMsg);
        return "main";
    }


    @GetMapping("/temp")
    public String temp(Model model) {
        String welcomeMsg = "SPA其他页面";
        log.info("cur welcomeMsg: {}", welcomeMsg);
        model.addAttribute("welcomeMsg", welcomeMsg);
        return "temp";
    }
}

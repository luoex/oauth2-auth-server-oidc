
服务规划 

|模块 | 协议 | 域名                     | 启动端口 | 服务描述 | 
|:--- |:-----|:-----------------------|:--- |:---| 
auth-server-prod | https | auth-server-prod       | 9000
scg-client-prod | https | scg-client-prod        | 8080
resource1-jwt-prod | http | resource1-jwt-prod     | 8081
resource2-jwt-prod | http | resource2-jwt-prod     | 8082
web-ajax-backend-prod | http | web-ajax-backend-prod  | 8091
spa-frontend-mock-prod | https | spa-frontend-mock-prod | 8092

SCG Client注册信息
> client_id: oauth2-scg-client-prod  
> client_secret: oauth2-scg-client-prod-secret  
> redirect_uri:   
> http://oauth2-scg-prod:8080/login/oauth2/code/oauth2-scg-client-prod  
> https://oauth2-scg-prod:8080/login/oauth2/code/oauth2-scg-client-prod

测试URL
> SCG Client登录触发端点：  
>https://scg-client-prod:8080/oauth2/authorization/scg-client-prod  
>https://scg-client-prod:8080/oauth2/authorization/scg-client-prod?redirect_uri=...
> 
> Web Ajax页面SCG代理请求：  
>https://scg-client-prod:8080/webAjax  
> Web Ajax页面直接请求：   
>http://web-ajax-backend-prod:8091
>
>
> SPA页面请求：  
> https://spa-frontend-mock-prod:8092/  
> SPA收到401后触发SCG登录请求：  
> https://scg-client-prod:8080/oauth2/authorization/scg-client-prod?redirect_uri=https://spa-frontend-mock-prod:8092
>
>资源服务SCG代理请求：  
>https://scg-client-prod:8080/resource1/articles  
>资源服务直接请求：    
>http://resource1-jwt-prod:8081/articles

## Keystore说明

> **注：** 以下keystore文件密码全部为123456

| keystore名称 | 域名Common Name | 说明 | 适用模块 |
|:---- |:---- |:---------|:----- |
keystore-mgt-all-in-one.p12 |  | 管理以下所有证书，以下证书均从此导出 | | 
ca.cer | luo-dev-ca | CA公钥证书（X.509），需安装到本地操作系统 | 本地操作系统 | 
ca.p12 | luo-dev-ca | CA公钥证书，用于访问https请求的客户端配置truststore，<br/>如scg-client-prod, resource1-jwt-prod通过https访问AuthServer issuer_uri | scg-client-prod, resource1-jwt-prod |
auth-server-prod.p12 | auth-server-prod | OAuth2 Auth Server 域名证书（公钥证书、私钥） | auth-server-prod | 
scg-client-prod.p12 | scg-client-prod | SCG Client 域名证书（公钥证书、私钥） | scg-client-prod |
spa-frontend-mock-prod.p12 | spa-frontend-mock-prod | SPA Mock 域名证书（公钥证书、私钥） | spa-frontend-mock-prod | 

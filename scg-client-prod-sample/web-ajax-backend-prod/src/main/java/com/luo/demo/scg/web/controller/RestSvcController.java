package com.luo.demo.scg.web.controller;

import com.luo.demo.scg.web.utils.HttpContextUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;
import java.util.Map;


/**
 * REST服务 - Controller
 *
 * @author luohq
 * @date 2022-02-18
 */
@RestController
@Slf4j
public class RestSvcController {

    private String[] articles = {"Article 1", "Article 2", "Article 3"};

    @GetMapping("/articles")
    public String[] getArticles() {
        this.logHeaders();
        this.logUserAndClientInfo();
        return this.articles;
    }

    @PostMapping("/articles")
    public String[] getArticlesPost() {
        this.logHeaders();
        this.logUserAndClientInfo();
        return this.articles;
    }

    /**
     * 打印请求头
     */
    private void logHeaders() {
        Map<String, String> headers = HttpContextUtils.getRequestHeaders();
        List<String> oauth2HeaderNames = Arrays.asList("authorization", "x-access-token", "x-id-token", "x-userinfo");
        headers.forEach((name, val) -> {
            if (oauth2HeaderNames.contains(name.toLowerCase())) {
                log.info("oauth2 header: {}: {}", name, val);
            }
        });
    }

    /**
     * 打印用户、客户端信息
     */
    private void logUserAndClientInfo() {
        //SystemUser systemuser = SystemUserContext.getUser();
        //log.info("cur osmium systemUser: {}", systemuser);
        //String clientId = ClientInfoContext.getClientInfo();
        //log.info("cur osmium clientId: {}", clientId);
    }
}

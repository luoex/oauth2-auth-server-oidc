package com.luo.demo.scg.client.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.BeanClassLoaderAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.security.jackson2.SecurityJackson2Modules;
import org.springframework.web.server.session.CookieWebSessionIdResolver;
import org.springframework.web.server.session.WebSessionIdResolver;

/**
 * @author jitendra on 3/3/16.
 */
@Configuration
public class SessionConfig implements BeanClassLoaderAware {

	private ClassLoader loader;

	/**
	 * 设置Redis Json序列化
	 * @return
	 */
	@Bean
	public RedisSerializer<Object> springSessionDefaultRedisSerializer() {
		return new GenericJackson2JsonRedisSerializer(objectMapper());
	}

	/**
	 * Customized {@link ObjectMapper} to add mix-in for class that doesn't have default
	 * constructors
	 * @return the {@link ObjectMapper} to use
	 */
	private ObjectMapper objectMapper() {
		ObjectMapper mapper = new ObjectMapper();
		mapper.registerModules(SecurityJackson2Modules.getModules(this.loader));
		return mapper;
	}

	/**
	 * @see BeanClassLoaderAware#setBeanClassLoader(ClassLoader)
	 */
	@Override
	public void setBeanClassLoader(ClassLoader classLoader) {
		this.loader = classLoader;
	}


	////webflux-cookie-serializer
	//@Bean
	//public WebSessionIdResolver webSessionIdResolver() {
	//	CookieWebSessionIdResolver resolver = new CookieWebSessionIdResolver();
	//	resolver.setCookieName("JSESSIONID");
	//	resolver.addCookieInitializer((builder) -> builder
	//			.path("/")
	//			.sameSite("Strict"));
	//	return resolver;
	//}

}

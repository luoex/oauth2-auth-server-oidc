package com.luo.demo.scg.client.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * 页面controller
 *
 * @author luohq
 * @version 1.0.0
 * @date 2022-02-14 14:52
 */
@Controller
@Slf4j
public class PageController {

    /**
     * 登录结果显示页，OAuth2 AuthServer登出后回调此页面
     *
     * @param model
     * @return
     */
    @GetMapping("/logout_result")
    public String logout_result(Model model) {
        String welcomeMsg = "已登出";
        log.info("Logout Redirect: {}", welcomeMsg);
        model.addAttribute("welcomeMsg", welcomeMsg);
        return "logout_result";
    }

}

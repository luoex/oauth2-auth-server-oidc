//package com.luo.demo.scg.client.config;
//
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.stereotype.Component;
//
//import javax.servlet.http.HttpSession;
//import javax.servlet.http.HttpSessionEvent;
//import javax.servlet.http.HttpSessionListener;
//
///**
// * Spring Session监听
// *
// * @author luohq
// * @version 1.0.0
// * @date 2022-03-08 17:14
// */
//@Component
//@Slf4j
//public class SpringSessionListener implements HttpSessionListener {
//    @Override
//    public void sessionCreated(HttpSessionEvent se) {
//        //HttpSessionAdaptor
//        HttpSession session = se.getSession();
//        log.info("session created - id:{}, maxInactiveInterval:{}", session.getId(), session.getMaxInactiveInterval());
//    }
//
//    @Override
//    public void sessionDestroyed(HttpSessionEvent se) {
//        HttpSession session = se.getSession();
//        log.info("session destroyed - id:{}", session.getId());
//    }
//}

package com.luo.demo.oidc.minimal;

import com.luo.sc.oidc.authserver.constant.Oauth2Constants.CLIENT_SETTINGS;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.EnabledIfSystemProperty;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.core.AuthorizationGrantType;
import org.springframework.security.oauth2.core.ClientAuthenticationMethod;
import org.springframework.security.oauth2.core.oidc.OidcScopes;
import org.springframework.security.oauth2.server.authorization.client.JdbcRegisteredClientRepository;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClient;
import org.springframework.security.oauth2.server.authorization.config.ClientSettings;
import org.springframework.test.context.ActiveProfiles;

import javax.annotation.Resource;
import java.util.UUID;

/**
 * OIDC 相关测试
 *
 * @author luohq
 * @date 2022-03-05 11:22
 */
@Slf4j
@SpringBootTest
@ActiveProfiles("oidc")
public class OidcTest {

    @Resource
    private JdbcRegisteredClientRepository registeredClientRepository;

    @Resource
    private PasswordEncoder passwordEncoder;

    @ParameterizedTest
    @ValueSource(strings = {"123456", "luo-oauth2-client1-secret", "luo-oauth2-client2-secret"})
    void testPasswordEncoder(String rawPassword) {
        log.info("rawPassword: {}, encodedPassword: {}", rawPassword, this.passwordEncoder.encode(rawPassword));
    }

    @Test
    //有条件测试，避免直接执行测试用例而导致无效插入
    @EnabledIfSystemProperty(named = "testRegOidcClient", matches = "true")
    void regOidcClient() {
        RegisteredClient registeredClient_client1 = RegisteredClient.withId(UUID.randomUUID().toString())
                .clientName("luo-oauth2-client1 - 客户端")
                .clientId("luo-oauth2-client1")
                .clientSecret("{bcrypt}$2a$10$LgGXHSU2Fh/dCLIwrOetiOnCK3Zypeo588EpAOQeJAnT0kdiia6em")
                .clientAuthenticationMethod(ClientAuthenticationMethod.CLIENT_SECRET_POST)
                .clientAuthenticationMethod(ClientAuthenticationMethod.CLIENT_SECRET_BASIC)
                .authorizationGrantType(AuthorizationGrantType.AUTHORIZATION_CODE)
                .authorizationGrantType(AuthorizationGrantType.REFRESH_TOKEN)
                .redirectUri("http://oauth2-client1:8081/login/oauth2/code/luo-oauth2-client1")
                .scope(OidcScopes.OPENID)
                .scope(OidcScopes.PHONE)
                .scope(OidcScopes.EMAIL)
                .scope(OidcScopes.PROFILE)
                .scope("articles.read")
                .scope("roles")
                .clientSettings(ClientSettings.builder()
                        .requireAuthorizationConsent(true)
                        //.requireProofKey(true)
                        .setting(CLIENT_SETTINGS.FRONTCHANNEL_LOGOUT_URI, "http://oauth2-client1:8081/front_logout")
                        .build())
                .build();


        RegisteredClient registeredClient_client2 = RegisteredClient.withId(UUID.randomUUID().toString())
                .clientName("luo-oauth2-client2 - 客户端")
                .clientId("luo-oauth2-client2")
                .clientSecret("{bcrypt}$2a$10$xw.pBLQitbNdAAyBhuBAo.IIP8dbDHcXF5c1YNIDqhhcC18cmNGo2")
                .clientAuthenticationMethod(ClientAuthenticationMethod.CLIENT_SECRET_POST)
                .clientAuthenticationMethod(ClientAuthenticationMethod.CLIENT_SECRET_BASIC)
                .authorizationGrantType(AuthorizationGrantType.AUTHORIZATION_CODE)
                .authorizationGrantType(AuthorizationGrantType.REFRESH_TOKEN)
                .redirectUri("http://oauth2-client2:8082/login/oauth2/code/luo-oauth2-client2")
                .scope(OidcScopes.OPENID)
                .scope(OidcScopes.PHONE)
                .scope(OidcScopes.EMAIL)
                .scope(OidcScopes.PROFILE)
                .scope("articles.read")
                .scope("roles")
                .clientSettings(ClientSettings.builder()
                        .requireAuthorizationConsent(true)
                        //.requireProofKey(true)
                        .setting(CLIENT_SETTINGS.FRONTCHANNEL_LOGOUT_URI, "http://oauth2-client2:8082/front_logout")
                        .build())
                .build();


        registeredClientRepository.save(registeredClient_client1);
        registeredClientRepository.save(registeredClient_client2);

    }

}


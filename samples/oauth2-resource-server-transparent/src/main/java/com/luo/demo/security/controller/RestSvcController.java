package com.luo.demo.security.controller;

import com.luo.demo.security.utils.HttpContextUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;
import java.util.Map;


/**
 * REST服务 - Controller
 *
 * @author luohq
 * @date 2022-02-18
 */
@RestController
@Slf4j
public class RestSvcController {

    private String[] articles = {"Article 1", "Article 2", "Article 3"};

    @GetMapping("/articles")
    public String[] getArticles() {
        logHeaders();
        return this.articles;
    }

    @PostMapping("/articles")
    public String[] getArticlesPost() {
        logHeaders();
        return this.articles;
    }

    private void logHeaders() {
        Map<String, String> headers = HttpContextUtils.getRequestHeaders();
        List<String> oauth2HeaderNames = Arrays.asList("authorization", "x-access-token", "x-id-token", "x-userinfo");
        headers.forEach((name, val) -> {
            if (oauth2HeaderNames.contains(name.toLowerCase())) {
                log.info("oauth2 header: {}: {}", name, val);
            }
        });
    }
}

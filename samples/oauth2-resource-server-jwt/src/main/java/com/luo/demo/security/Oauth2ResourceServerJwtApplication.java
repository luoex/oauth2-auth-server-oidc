package com.luo.demo.security;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 启动应用
 *
 * @author luohq
 * @date 2022-01-15 12:01
 */
@SpringBootApplication
public class Oauth2ResourceServerJwtApplication {
    public static void main(String[] args) {
        SpringApplication.run(Oauth2ResourceServerJwtApplication.class, args);
    }
}

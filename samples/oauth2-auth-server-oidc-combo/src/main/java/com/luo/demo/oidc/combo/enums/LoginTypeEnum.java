package com.luo.demo.oidc.combo.enums;

/**
 * 登录类型枚举
 *
 * @author luohq
 * @version 1.0.0
 * @date 2022-03-07 12:40
 */
public enum LoginTypeEnum {
    USERNAME(1, "用户名密码登录"),
    PHONE(2, "手机号登录");

    private Integer type;
    private String desc;

    LoginTypeEnum(Integer type, String desc) {
        this.type = type;
        this.desc = desc;
    }

    public Integer getType() {
        return type;
    }

    public String getTypeStr() {
        return String.valueOf(this.type);
    }

    public String getDesc() {
        return desc;
    }

}

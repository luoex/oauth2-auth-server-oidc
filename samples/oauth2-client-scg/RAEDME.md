
- http://oauth2-scg:8083/resource1/articles
  - http://oauth2-resource1:8090/articles


启动SCG并且作为OAuth2 Client，
即前端请求SCG时，若当前session未认证，则重定向到OAuth2授权端点进行认证，
作用类似于OAuth2 Login功能，
登出时由前端触发http://oauth2-scg:8083/logout，进而登出当前session，同时触发OIDC统一登出逻辑。


>注：
> SCG作为OAuth2 Client需添加TokenRelay过滤器，如此可将SCG Client端存储的access_token透传到后端服务
> Authorization: Bearer {access_token}
> 需注意共享session存储问题。
> 需注意SCG 刷新token逻辑？
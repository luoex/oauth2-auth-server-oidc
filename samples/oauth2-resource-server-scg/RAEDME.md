
- http://oauth2-scg:8070/resource1/articles
  - http://oauth2-resource1:8090/articles


启动SCG且使用JWT验证方式
-Dspring.profiles.active=scg,oauth2-resource-server-jwt



启动SCG且使用OpaqueToken验证方式
-Dspring.profiles.active=scg,oauth2-resource-server-opaque


>注：
> SCG作为OAuth2 Resource Server会默认透传请求头到后端服务：
> Authorization: Bearer {access_token}
package com.luo.demo.scg.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 启动应用
 *
 * @author luohq
 * @date 2022-01-15 12:01
 */
@SpringBootApplication
public class Oauth2ScgResourceServerApplication {
    public static void main(String[] args) {
        SpringApplication.run(Oauth2ScgResourceServerApplication.class, args);
    }
}

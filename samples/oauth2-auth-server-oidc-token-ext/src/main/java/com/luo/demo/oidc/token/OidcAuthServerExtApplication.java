package com.luo.demo.oidc.token;

import com.luo.sc.oidc.authserver.anno.EnableOidcAuthorizationServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * OIDC Server启动类
 *
 * @author luo
 * @date 2022-02-17
 */
@EnableOidcAuthorizationServer
@SpringBootApplication
public class OidcAuthServerExtApplication {

	public static void main(String[] args) {
		SpringApplication.run(OidcAuthServerExtApplication.class, args);
	}

}
package org.springframework.security.oauth2.server.authorization.authentication;

import com.luo.sc.oidc.authserver.enums.AuthenticaionResultCodeEnum;
import com.luo.sc.oidc.authserver.handler.login.UniLoginAuthenticationToken;
import com.luo.sc.oidc.authserver.handler.login.UniLoginUserDetails;
import com.luo.sc.oidc.authserver.handler.login.UniLoginUserDetailsService;
import com.nimbusds.openid.connect.sdk.OIDCScopeValue;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.keygen.Base64StringKeyGenerator;
import org.springframework.security.crypto.keygen.StringKeyGenerator;
import org.springframework.security.oauth2.core.*;
import org.springframework.security.oauth2.core.oidc.OidcIdToken;
import org.springframework.security.oauth2.core.oidc.OidcScopes;
import org.springframework.security.oauth2.core.oidc.endpoint.OidcParameterNames;
import org.springframework.security.oauth2.jwt.JoseHeader;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.jwt.JwtClaimsSet;
import org.springframework.security.oauth2.jwt.JwtEncoder;
import org.springframework.security.oauth2.server.authorization.JwtEncodingContext;
import org.springframework.security.oauth2.server.authorization.OAuth2Authorization;
import org.springframework.security.oauth2.server.authorization.OAuth2AuthorizationService;
import org.springframework.security.oauth2.server.authorization.OAuth2TokenCustomizer;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClient;
import org.springframework.security.oauth2.server.authorization.config.ProviderSettings;
import org.springframework.security.oauth2.server.authorization.context.ProviderContextHolder;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;

import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Supplier;

import static org.springframework.security.oauth2.server.authorization.authentication.OAuth2AuthenticationProviderUtils.getAuthenticatedClientElseThrowInvalidClient;

/**
 * OAuth 2.0 Authorization Password Grant
 *
 * @author luohq
 * @date 2022-03-10
 * @see OAuth2AuthorizationCodeAuthenticationProvider
 * @see OAuth2ClientCredentialsAuthenticationProvider
 */
public final class OAuth2CustomPassowordAuthenticationProvider implements AuthenticationProvider {
	private static final OAuth2TokenType ID_TOKEN_TOKEN_TYPE =
			new OAuth2TokenType(OidcParameterNames.ID_TOKEN);
	private static final StringKeyGenerator DEFAULT_REFRESH_TOKEN_GENERATOR =
			new Base64StringKeyGenerator(Base64.getUrlEncoder().withoutPadding(), 96);
	private final OAuth2AuthorizationService authorizationService;
	private final JwtEncoder jwtEncoder;
	private OAuth2TokenCustomizer<JwtEncodingContext> jwtCustomizer = (context) -> {};
	private Supplier<String> refreshTokenGenerator = DEFAULT_REFRESH_TOKEN_GENERATOR::generateKey;

	private UniLoginUserDetailsService uniLoginUserDetailsService;
	/**
	 * Constructs an {@code OAuth2AuthorizationCodeAuthenticationProvider} using the provided parameters.
	 *
	 * @param authorizationService the authorization service
	 * @param jwtEncoder the jwt encoder
	 */
	public OAuth2CustomPassowordAuthenticationProvider(OAuth2AuthorizationService authorizationService,
													   UniLoginUserDetailsService uniLoginUserDetailsService,
													   JwtEncoder jwtEncoder) {
		Assert.notNull(authorizationService, "authorizationService cannot be null");
		Assert.notNull(uniLoginUserDetailsService, "uniLoginUserDetailsService cannot be null");
		Assert.notNull(jwtEncoder, "jwtEncoder cannot be null");
		this.authorizationService = authorizationService;
		this.uniLoginUserDetailsService = uniLoginUserDetailsService;
		this.jwtEncoder = jwtEncoder;
	}

	/**
	 * Sets the {@link OAuth2TokenCustomizer} that customizes the
	 * {@link JwtEncodingContext.Builder#headers(Consumer) headers} and/or
	 * {@link JwtEncodingContext.Builder#claims(Consumer) claims} for the generated {@link Jwt}.
	 *
	 * @param jwtCustomizer the {@link OAuth2TokenCustomizer} that customizes the headers and/or claims for the generated {@code Jwt}
	 */
	public void setJwtCustomizer(OAuth2TokenCustomizer<JwtEncodingContext> jwtCustomizer) {
		Assert.notNull(jwtCustomizer, "jwtCustomizer cannot be null");
		this.jwtCustomizer = jwtCustomizer;
	}

	/**
	 * Sets the {@code Supplier<String>} that generates the value for the {@link OAuth2RefreshToken}.
	 *
	 * @param refreshTokenGenerator the {@code Supplier<String>} that generates the value for the {@link OAuth2RefreshToken}
	 */
	public void setRefreshTokenGenerator(Supplier<String> refreshTokenGenerator) {
		Assert.notNull(refreshTokenGenerator, "refreshTokenGenerator cannot be null");
		this.refreshTokenGenerator = refreshTokenGenerator;
	}

	@Deprecated
	protected void setProviderSettings(ProviderSettings providerSettings) {
	}

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		OAuth2PasswordAuthenticationToken passwordAuthenticationToken =
				(OAuth2PasswordAuthenticationToken) authentication;
		//private String clientId =

		OAuth2ClientAuthenticationToken clientPrincipal =
				getAuthenticatedClientElseThrowInvalidClient(passwordAuthenticationToken);

		RegisteredClient registeredClient = clientPrincipal.getRegisteredClient();

		/** Password授权模式验证，参考自：OAuth2ClientCredentialsAuthenticationProvider验证逻辑 */
		if (!registeredClient.getAuthorizationGrantTypes().contains(AuthorizationGrantType.PASSWORD)) {
			throw new OAuth2AuthenticationException(OAuth2ErrorCodes.UNAUTHORIZED_CLIENT);
		}

		// Default to configured scopes
		Set<String> authorizedScopes = registeredClient.getScopes();
		if (!CollectionUtils.isEmpty(passwordAuthenticationToken.getScopes())) {
			for (String requestedScope : passwordAuthenticationToken.getScopes()) {
				if (!registeredClient.getScopes().contains(requestedScope)) {
					throw new OAuth2AuthenticationException(OAuth2ErrorCodes.INVALID_SCOPE);
				}
			}
			authorizedScopes = new LinkedHashSet<>(passwordAuthenticationToken.getScopes());
		}

		/** 调用自定义用户验证逻辑，参考自：UniLoginAuthenticationProvider */
		UniLoginUserDetails userDetails = null;
		UniLoginAuthenticationToken authResult = null;
		try {
			//根据认证表单参数查询用户信息
			userDetails = this.uniLoginUserDetailsService.loadUserByAuthParams(passwordAuthenticationToken.getAuthParams());
			///验证用户信息
			this.uniLoginUserDetailsService.authenticateUser(passwordAuthenticationToken.getAuthParams(), userDetails);
			//构建验证成功结果
			authResult = new UniLoginAuthenticationToken(userDetails, userDetails.getPassword(),
					userDetails.getAuthorities(), passwordAuthenticationToken.getAuthParams());
			//移除authParams，避免在session中可以看见密码等信息
			//passwordAuthenticationToken.setAuthParams(null);
		} catch (Throwable ex) {
			/** 转换成OAuth2AuthenticationException，否则OAuth2TokenEndpointFilter无法捕获处理 */
			throw new OAuth2AuthenticationException(new OAuth2Error(String.valueOf(AuthenticaionResultCodeEnum.FAILURE.getCode()), ex.getMessage(), null));
		}


		/** 生成access_token, refresh_token，参考自：OAuth2CustomAuthorizationCodeAuthenticationProvider, OAuth2ClientCredentialsAuthenticationProvider*/
		String issuer = ProviderContextHolder.getProviderContext().getIssuer();
		//Set<String> authorizedScopes = authorization.getAttribute(
		//		OAuth2Authorization.AUTHORIZED_SCOPE_ATTRIBUTE_NAME);

		JoseHeader.Builder headersBuilder = JwtUtils.headers();
		JwtClaimsSet.Builder claimsBuilder = JwtUtils.accessTokenClaims(
				registeredClient, issuer, userDetails.getUsername(),
				authorizedScopes);

		// @formatter:off
		JwtEncodingContext context = JwtEncodingContext.with(headersBuilder, claimsBuilder)
				.registeredClient(registeredClient)
				.principal(authResult)
				//.authorization(authorization)
				.authorizedScopes(authorizedScopes)
				.tokenType(OAuth2TokenType.ACCESS_TOKEN)
				.authorizationGrantType(AuthorizationGrantType.PASSWORD)
				.authorizationGrant(passwordAuthenticationToken)
				.build();
		// @formatter:on

		this.jwtCustomizer.customize(context);

		JoseHeader headers = context.getHeaders().build();
		JwtClaimsSet claims = context.getClaims().build();
		Jwt jwtAccessToken = this.jwtEncoder.encode(headers, claims);

		OAuth2AccessToken accessToken = new OAuth2AccessToken(OAuth2AccessToken.TokenType.BEARER,
				jwtAccessToken.getTokenValue(), jwtAccessToken.getIssuedAt(),
				jwtAccessToken.getExpiresAt(), authorizedScopes);

		/** =========== 修改逻辑 - 开始 =========== **/
		OAuth2RefreshToken refreshToken = null;
		if (registeredClient.getAuthorizationGrantTypes().contains(AuthorizationGrantType.REFRESH_TOKEN) &&
				// issue refresh token to confidential client Or public client who owns scope offline_access
				(!clientPrincipal.getClientAuthenticationMethod().equals(ClientAuthenticationMethod.NONE) || authorizedScopes.contains(OIDCScopeValue.OFFLINE_ACCESS.getValue()))) {
			refreshToken = generateRefreshToken(registeredClient.getTokenSettings().getRefreshTokenTimeToLive());
		}
		/** =========== 修改逻辑 - 结束 =========== **/


		/** id_token无法获取nonce参数，原生成id_token逻辑可参见：OAuth2CustomAuthorizationCodeAuthenticationProvider */
		Jwt jwtIdToken = null;
		if (authorizedScopes.contains(OidcScopes.OPENID)) {
			//无法获取nonce参数
			//String nonce = (String) authorizationRequest.getAdditionalParameters().get(OidcParameterNames.NONCE);
			String nonce = null;

			headersBuilder = JwtUtils.headers();
			claimsBuilder = JwtUtils.idTokenClaims(
					registeredClient, issuer, userDetails.getUsername(), nonce);

			// @formatter:off
			context = JwtEncodingContext.with(headersBuilder, claimsBuilder)
					.registeredClient(registeredClient)
					.principal(authResult)
					//.authorization(authorization)
					.authorizedScopes(authorizedScopes)
					.tokenType(ID_TOKEN_TOKEN_TYPE)
					.authorizationGrantType(AuthorizationGrantType.PASSWORD)
					.authorizationGrant(passwordAuthenticationToken)
					.build();
			// @formatter:on

			this.jwtCustomizer.customize(context);

			headers = context.getHeaders().build();
			claims = context.getClaims().build();
			jwtIdToken = this.jwtEncoder.encode(headers, claims);
		}

		OidcIdToken idToken;
		if (jwtIdToken != null) {
			idToken = new OidcIdToken(jwtIdToken.getTokenValue(), jwtIdToken.getIssuedAt(),
					jwtIdToken.getExpiresAt(), jwtIdToken.getClaims());
		} else {
			idToken = null;
		}

		// @formatter:off
		OAuth2Authorization.Builder authorizationBuilder = OAuth2Authorization.withRegisteredClient(registeredClient)
				.principalName(userDetails.getUsername())
				.authorizationGrantType(AuthorizationGrantType.PASSWORD)
				.token(accessToken,
						(metadata) ->
								metadata.put(OAuth2Authorization.Token.CLAIMS_METADATA_NAME, jwtAccessToken.getClaims())
				);
		if (refreshToken != null) {
			authorizationBuilder.refreshToken(refreshToken);
		}
		if (idToken != null) {
			authorizationBuilder
					.token(idToken,
							(metadata) ->
									metadata.put(OAuth2Authorization.Token.CLAIMS_METADATA_NAME, idToken.getClaims()));
		}
		OAuth2Authorization authorization = authorizationBuilder.build();
		// @formatter:on

		// Invalidate the authorization code as it can only be used once
		//authorization = OAuth2AuthenticationProviderUtils.invalidate(authorization, authorizationCode.getToken());

		this.authorizationService.save(authorization);

		Map<String, Object> additionalParameters = Collections.emptyMap();
		if (idToken != null) {
			additionalParameters = new HashMap<>();
			additionalParameters.put(OidcParameterNames.ID_TOKEN, idToken.getTokenValue());
		}

		////TODO 是否需要设置SecurityContext，以及之前的SecurityContext具体值
		//SecurityContext securityContext = SecurityContextHolder.createEmptyContext();
		//securityContext.setAuthentication(authResult);
		//SecurityContextHolder.setContext(securityContext);
		return new OAuth2AccessTokenAuthenticationToken(
				registeredClient, clientPrincipal, accessToken, refreshToken, additionalParameters);
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return OAuth2PasswordAuthenticationToken.class.isAssignableFrom(authentication);
	}

	private OAuth2RefreshToken generateRefreshToken(Duration tokenTimeToLive) {
		Instant issuedAt = Instant.now();
		Instant expiresAt = issuedAt.plus(tokenTimeToLive);
		return new OAuth2RefreshToken(this.refreshTokenGenerator.get(), issuedAt, expiresAt);
	}

}

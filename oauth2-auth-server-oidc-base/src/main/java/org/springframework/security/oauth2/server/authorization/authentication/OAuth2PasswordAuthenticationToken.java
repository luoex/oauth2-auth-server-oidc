package org.springframework.security.oauth2.server.authorization.authentication;

import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.springframework.lang.Nullable;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.core.AuthorizationGrantType;

/**
 * An {@link Authentication} implementation used for the OAuth 2.0 Password Grant.
 *
 * @author luohq
 * @since 0.0.1
 * @see OAuth2AuthorizationGrantAuthenticationToken
 * @see OAuth2ClientCredentialsAuthenticationProvider
 */
public class OAuth2PasswordAuthenticationToken extends OAuth2AuthorizationGrantAuthenticationToken {
	private final Set<String> scopes;
	private final Map<String, String> authParams;

	/**
	 * Constructs an {@code OAuth2ClientCredentialsAuthenticationToken} using the provided parameters.
	 *
	 * @param clientPrincipal the authenticated client principal
	 * @param scopes the requested scope(s)
	 * @param authParams the additional parameters
	 */
	public OAuth2PasswordAuthenticationToken(Authentication clientPrincipal,
											 Map<String, String> authParams, @Nullable Set<String> scopes) {
		super(AuthorizationGrantType.PASSWORD, clientPrincipal, null);
		this.authParams = authParams;
		this.scopes = Collections.unmodifiableSet(
				scopes != null ? new HashSet<>(scopes) : Collections.emptySet());
	}

	/**
	 * Returns the requested scope(s).
	 *
	 * @return the requested scope(s), or an empty {@code Set} if not available
	 */
	public Set<String> getScopes() {
		return this.scopes;
	}

	public Map<String, String> getAuthParams() {
		return authParams;
	}
}
package com.luo.sc.oidc.authserver.handler.kapatcha;

import com.google.code.kaptcha.Constants;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Kaptcha 生成结果保存接口
 *
 * @author luohq
 * @version 1.0.0
 * @date 2022-03-14 14:48
 */
public interface KapatchResultSaveService {

    /**
     * 保存图片验证码结果文本（用于后续提交验证）
     *
     * @param resultText 验证码结果文本
     * @param request    请求对象
     * @param response   响应对象
     */
    void saveResult(String resultText, HttpServletRequest request, HttpServletResponse response);


    /**
     * 用于获取之前保存的保存图片验证码结果文本
     *
     * @param request  请求对象
     * @param response 响应对象
     * @return 返回之前缓存的验证码结果文本
     */
    String getResult(HttpServletRequest request, HttpServletResponse response);

    /**
     * 清除之前缓存的结果文本
     *
     * @param request  请求对象
     * @param response 响应对象
     */
    void clearResult(HttpServletRequest request, HttpServletResponse response);

    /**
     * 验证结果文本是否匹配（默认验证匹配成功后即清除缓存的结果文本）
     *
     * @param resultText 验证码结果文本
     * @param request    请求对象
     * @param response   响应对象
     * @return true匹配，false则不匹配
     */
    Boolean verifyResult(String resultText, HttpServletRequest request, HttpServletResponse response);

    /**
     * 验证结果文本是否匹配
     *
     * @param resultText       验证码结果文本
     * @param request          请求对象
     * @param response         响应对象
     * @param clearCacheResult 清除之前缓存的验证码结果文本
     * @return true匹配，false则不匹配
     */
    Boolean verifyResult(String resultText, HttpServletRequest request, HttpServletResponse response, Boolean clearCacheResult);
}

package com.luo.sc.oidc.authserver.handler.kapatcha;

import lombok.Builder;
import lombok.Data;

import java.awt.image.BufferedImage;

/**
 * 验证码生成结果 - DTO
 *
 * @author luohq
 * @version 1.0.0
 * @date 2022-03-14 14:40
 */
@Data
@Builder
public class KaptchaResultDto {
    /**
     * 渲染文本（显示给终端用户的文本）
     */
    private String renderText;
    /**
     * 实际结果文本（用来跟终端用户提交的结果进行比对）
     */
    private String resultText;
    /**
     * 生成的验证码图片（显示内容即为renderText）
     */
    BufferedImage kaptchaImage;
}

package com.luo.sc.oidc.authserver.handler.login;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.SpringSecurityCoreVersion;
import org.springframework.util.Assert;

import java.util.Collection;
import java.util.Map;

/**
 * 短信验证码 - 认证信息
 *
 * @author luohq
 * @version 1.0.0
 * @date 2022-03-03、
 * @see org.springframework.security.authentication.UsernamePasswordAuthenticationToken
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS)
@JsonIgnoreProperties({"name", "authParams"})
public class UniLoginAuthenticationToken extends AbstractAuthenticationToken {

    private static final long serialVersionUID = SpringSecurityCoreVersion.SERIAL_VERSION_UID;

    private Object principal;
    private Object credentials;


    private Map<String, String> authParams;

    public UniLoginAuthenticationToken() {
        super(null);
    }


    /**
     * 未认证通过时的认证信息（仅包含认证form参数）
     *
     * @param authParams 认证form参数
     */
    public UniLoginAuthenticationToken(Map<String, String> authParams) {
        super(null);
        this.authParams = authParams;
        super.setAuthenticated(false);
    }

    /**
     * 认证通过后的认证信息（包含完整的认证信息）
     *
     * @param principal   用户身份或用户唯一标识（必须有值）
     * @param credentials 用户凭证
     * @param authorities 用户权限
     * @param authParams  认证form参数
     */
    public UniLoginAuthenticationToken(Object principal, Object credentials, Collection<? extends GrantedAuthority> authorities, Map<String, String> authParams) {
        super(authorities);
        Assert.notNull(principal, "Authentication Principal must not be null!");
        this.principal = principal;
        this.credentials = credentials;
        this.authParams = authParams;
        // must use super, as we override
        super.setAuthenticated(true);
    }


    @Override
    public Object getCredentials() {
        return this.credentials;
    }

    @Override
    public Object getPrincipal() {
        return this.principal;
    }

    @Override
    public void eraseCredentials() {
        super.eraseCredentials();
    }


    public Map<String, String> getAuthParams() {
        return authParams;
    }

    public void setAuthParams(Map<String, String> authParams) {
        this.authParams = authParams;
    }
}
package com.luo.sc.oidc.authserver.enums;

/**
 * 图片验证码类型
 *
 * @author luohq
 * @version 1.0.0
 * @date 2022-02-21 11:10
 */
public enum KaptchaTypeEnum {
    //文本字符串
    TEXT(1, "text"),
    //算术表达式
    MATH(2, "math");

    /**
     * 类型
     */
    private Integer code;
    /**
     * 类型描述
     */
    private String desc;

    KaptchaTypeEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public Integer getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}

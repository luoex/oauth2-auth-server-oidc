package com.luo.sc.oidc.authserver.handler.login;

import lombok.Builder;
import lombok.Data;

/**
 * 通用登录 - 响应结果 - DTO
 *
 * @author luohq
 * @version 1.0.0
 * @date 2022-03-11 16:19
 */
@Data
@Builder
public class UniLoginRespJsonDto {
    /**
     * 认证结果响应码（100成功, 101失败）
     */
    private String code;
    /**
     * 响应结果描述信息
     */
    private String msg;
    /**
     * 认证成功后需前端对应的重定向Uri
     */
    private String redirectUri;
}

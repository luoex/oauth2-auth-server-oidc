package com.luo.sc.oidc.authserver.anno;

import com.luo.sc.oidc.authserver.config.AuthorizationServerConfig;
import com.luo.sc.oidc.authserver.config.FormSecurityConfig;
import com.luo.sc.oidc.authserver.handler.kapatcha.config.KaptchaConfig;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;


/**
 * 启用OIDC Authorization Server
 *
 * @author luohq
 * @version 1.0.0
 * @date 2022-02-21 10:35
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Documented
@Import({AuthorizationServerConfig.class, FormSecurityConfig.class, KaptchaConfig.class,
        AuthorizationServerConfig.ConsentPageConfig.class, FormSecurityConfig.LoginPageConfig.class})
public @interface EnableOidcAuthorizationServer {

}
package com.luo.sc.oidc.authserver.handler.token;

import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

/**
 * Token Endpoint 响应结果处理器
 *
 * @author luohq
 * @version 1.0.0
 * @date 2022-03-16 09:50
 */
public interface OAuth2TokenEndpointAuthenticationResponseHandler extends AuthenticationSuccessHandler, AuthenticationFailureHandler {
}

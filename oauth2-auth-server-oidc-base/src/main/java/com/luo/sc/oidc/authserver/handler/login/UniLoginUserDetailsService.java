package com.luo.sc.oidc.authserver.handler.login;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Map;


/**
 * 通用登录 - 用户查询及认证服务
 *
 * @author luohq
 * @version 1.0.0
 * @date 2022-03-03
 * @see org.springframework.security.core.userdetails.UserDetailsService
 * @see org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider
 */
public interface UniLoginUserDetailsService {


    /**
     * 根据认证form参数查询用户信息
     *
     * @param authParams form参数
     * @return 用户信息
     * @throws UsernameNotFoundException
     */
    UniLoginUserDetails loadUserByAuthParams(Map<String, String> authParams) throws UsernameNotFoundException;

    /**
     * 认证用户（即form参数和用户信息中的凭证是否匹配等）
     *
     * @param authParams          form参数
     * @param uniLoginUserDetails 用户信息
     * @throws AuthenticationException
     */
    void authenticateUser(Map<String, String> authParams, UniLoginUserDetails uniLoginUserDetails) throws AuthenticationException;

}

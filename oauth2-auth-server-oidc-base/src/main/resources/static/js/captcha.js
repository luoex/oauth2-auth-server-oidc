//基础验证码图片url
const captchaUrlBase = document.getElementById("captcha-addon").getAttribute("src")
/**
 * 刷新验证码
 */
function switchCaptcha() {
    let url = captchaUrlBase + "?s=" + Math.random();
    document.getElementById("captcha-addon").src = url;
}
